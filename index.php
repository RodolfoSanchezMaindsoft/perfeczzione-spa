<!--
    * Nombre: index.php
    * Creado por: Luis Cristerna (MAINDSOFT SISTEMAS INTEGRALES)
    * Fecha: 12/03/2019
-->
<?php include("php/components.php") ?>
<!DOCTYPE html>
<html lang="en" class="font-primary">
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126808317-5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-126808317-5');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1054439918278264');
        fbq('track', 'PageView');
    </script>

    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=1054439918278264&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->


    <title>Perfeczzione | SPA</title>

    <!--Meta Tags-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!--Autor de la pagina-->
    <meta name="author" content="MAINDSOFT SISTEMAS INTEGRALES">
    <!--Palabras clave de la pagina (Importante en el seo)-->
    <meta name="keywords" content="Perfeczzione,SPA,Aguascalientes,Belleza,Masajes,Faciales">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta name="description"
          content="Es uno de los lugares más recomendables para liberarte de las preocupaciones y dejarte querer.">
    <!--Meta imagen (Imagen que se muestra al compartir la pagina)-->
    <meta name="image" content="https://perfeczzione.mx/assets/global/img/meta-img/meta-img.png">
    <!--Color de la pestana de la pagina (Utilizada en chrome para moviles, por ejemplo)-->
    <meta name="theme-color" content="#FFFFFF">
    <!-- Meta Tags (Facebook, Pinterest & Google+) -->
    <!--Titulo de la pagina-->
    <meta property="og:title" content="Perfeczzione | SPA">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta property="og:description"
          content="Es uno de los lugares más recomendables para liberarte de las preocupaciones y dejarte querer.">
    <!--URL de la pagina-->
    <meta property="og:url" content="https://perfeczzione.mx/">
    <!--Nombre de la pagina-->
    <meta property="og:site_name" content="Perfeczzione | SPA">
    <!--De que pais es la pagina, mas no el servidor-->
    <meta property="og:locale" content="es_MX">
    <!--Tipo de la pagina: sitio web-->
    <meta property="og:type" content="website"/>
    <!--Imagen al compartir-->
    <meta property="og:image" content="https://perfeczzione.mx/assets/global/img/meta-img/meta-img.png"/>
    <!--ID FB-->
    <meta property="fb:app_id" content="1180674568774149"/>
    <!-- Meta Tags Google -->
    <!--Titulo de la pagina-->
    <meta itemprop="name" content="Perfeczzione | SPA">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta itemprop="description"
          content="Es uno de los lugares más recomendables para liberarte de las preocupaciones y dejarte querer.">
    <!--End Meta Tags-->

    <!-- Fav-->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Fuentes necesarias para la pagina -->
    <link href="https://fonts.googleapis.com/css?family=Blinker|Dancing+Script&display=swap" rel="stylesheet">

    <!-- CSS -->

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/global/css/bootstrap/bootstrap.min.css">

    <!-- Plugins -->
    <!-- Banderas de Paises-->
    <link rel="stylesheet" href="assets/global/css/icons/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/global/css/icons/icon-hs/style.css">
    <link rel="stylesheet" href="assets/global/css/hamburgers/hamburgers.min.css">

    <!-- Revolution Slider -->
    <link rel="stylesheet"
          href="assets/global/css/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="assets/global/css/revolution-slider/revolution/css/settings.min.css">
    <link rel="stylesheet" href="assets/global/css/revolution-slider/revolution/css/layers.min.css">
    <link rel="stylesheet" href="assets/global/css/revolution-slider/revolution/css/navigation.min.css">

    <!-- jquery-ui -->
    <link rel="stylesheet" href="assets/global/css/jquery-ui/jquery-ui.min.css">

    <!-- FancyBox -->
    <link rel="stylesheet" href="assets/global/css/fancybox/jquery.fancybox.min.css">

    <!-- Master Slider -->
    <link rel="stylesheet" href="assets/global/css/master-slider/masterslider.main.css">
    <link rel="stylesheet" href="assets/global/css/master-slider/master-slider-style.css">
    <link rel="stylesheet" href="assets/global/css/jquery-ui/jquery-ui.min.css">

    <!-- Unify -->
    <link rel="stylesheet" href="assets/global/css/unify/unify.min.css">

    <!-- End CSS -->
</head>

<body>
<main>

    <!-- Header -->
    <header id="js-header" class="u-header u-header--sticky-top u-header--change-appearance g-z-index-9999"
            data-header-fix-moment="100">
        <div class="light-theme u-header__section g-transition-0_3 g-py-6 g-py-14--md"
             data-header-fix-moment-exclude="light-theme g-py-14--md"
             data-header-fix-moment-classes="dark-theme u-shadow-v27 g-bg-white g-py-11--md">
            <nav class="navbar navbar-expand-lg g-py-0">
                <div class="container g-pos-rel">
                    <!-- Logo -->
                    <a href="#!" class="js-go-to navbar-brand u-header__logo"
                       data-type="static">
                        <img class="u-header__logo-img u-header__logo-img--main d-block g-width-150" src="logo.png"
                             alt="Image description"
                             data-header-fix-moment-exclude="d-block"
                             data-header-fix-moment-classes="d-none">

                        <img class="u-header__logo-img u-header__logo-img--main d-none g-width-150" src="logo.png"
                             alt="Image description"
                             data-header-fix-moment-exclude="d-none"
                             data-header-fix-moment-classes="d-block"> </a>
                    <!-- End Logo -->

                    <!-- Navigation -->
                    <div class="collapse navbar-collapse align-items-center flex-sm-row" id="navBar"
                         data-mobile-scroll-hide="true">
                        <ul id="js-scroll-nav"
                            class="navbar-nav text-uppercase g-font-weight-700 g-font-size-11 g-pt-20 g-pt-0--lg ml-auto">
                            <li class="nav-item g-mx-12--lg g-mb-7 g-mb-0--lg">
                                <a href="#inicio" class="nav-link p-0">inicio</a>
                            </li>
                            <li class="nav-item g-mx-12--lg g-mb-7 g-mb-0--lg">
                                <a href="#servicios" class="nav-link p-0">servicios</a>
                            </li>
                            <li class="nav-item g-mx-12--lg g-mb-7 g-mb-0--lg">
                                <a href="#promociones-paquetes" class="nav-link p-0">Paquetes</a>
                            </li>
                            <li class="nav-item g-mx-12--lg g-mb-7 g-mb-0--lg">
                                <a href="#cotizar" class="nav-link p-0">Cotizar</a>
                            </li>
                            <li class="nav-item g-mx-12--lg g-mb-7 g-mb-0--lg">
                                <a href="#contacto" class="nav-link p-0">Contacto</a>
                            </li>
                            <li class="nav-item g-mx-12--lg g-mb-7 g-mb-0--lg">
                                <a href="#mapa" class="nav-link p-0">Mapa</a>
                            </li>
                            <li class="nav-item g-mx-12--lg g-mb-7 g-mb-0--lg">
                                <a href="https://www.facebook.com/pages/category/Health-Spa/Perfeczzione-105530587468679/" target="_blank"
                                   class="nav-link p-0"><span class="fa fa-facebook"></span></a>
                            </li>
                            <li class="nav-item g-mx-12--lg g-mb-7 g-mb-0--lg">
                                <a href="https://www.instagram.com/perfeczzione.ags/" target="_blank"
                                   class="nav-link p-0"><span class="fa fa-instagram"></span></a>
                            </li>
                        </ul>
                    </div>
                    <!-- End Navigation -->

                    <!-- Responsive Toggle Button -->
                    <button class="navbar-toggler btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-10 g-right-0"
                            type="button"
                            aria-label="Toggle navigation"
                            aria-expanded="false"
                            aria-controls="navBar"
                            data-toggle="collapse"
                            data-target="#navBar">
                <span class="hamburger hamburger--slider">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </span>
                    </button>
                    <!-- End Responsive Toggle Button -->
                </div>
            </nav>
        </div>
    </header>
    <!-- End Header -->


    <div id="inicio">
        <div id="rev_slider_1063_1_wrapper" class="rev_slider_wrapper fullscreen-container"
             style="background-color: transparent; padding: 0; margin: 0 auto;"
             data-alias="highlight-showcase122"
             data-source="gallery">
            <div id="rev_slider_1063_1" class="rev_slider fullscreen-container" style="display: none;"
                 data-version="5.4.1">
                <ul>
                    <li data-index="rs-2985"
                        data-transition="slideoverhorizontal"
                        data-slotamount="7"
                        data-hideafterloop="0"
                        data-hideslideonmobile="off"
                        data-easein="default"
                        data-easeout="default"
                        data-masterspeed="1500"
                        data-thumb="assets/img/newspaper_bg1-100x50.jpg"
                        data-rotate="0"
                        data-fstransition="fade"
                        data-fsmasterspeed="1000"
                        data-fsslotamount="7"
                        data-saveperformance="off"
                    >
                        <img class="rev-slidebg" src="assets/global/img/index/index-fondo-principal.jpg"
                             alt="Image description"
                             data-bgposition="center bottom"
                             data-bgfit="cover"
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10">

                        <img class="rev-slidebg" src="assets/global/img/index/index-fondo-principal-capa-2.png"
                             alt="Image description"
                             data-bgposition="center bottom"
                             data-bgfit="cover"
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10">

                        <!-- LAYER NR. 10 -->
                        <div id="slide-964-layer-42" class="tp-caption tp-resizeme rs-parallaxlevel-4"
                             style="z-index: 14;"
                             data-x="['center','center','center','center']"
                             data-y="['center','center','center','center']"
                             data-hoffset="['-350','-250','-150','-80']"
                             data-voffset="['-100','0','0','50']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-type="image"
                             data-beforeafter="after"
                             data-responsive_offset="on"
                             data-frames='[
                     {"delay":1300,"speed":1300,"frame":"0","from":"x:right;y:-500px;rZ:-180deg;","to":"o:1;","ease":"Power4.easeOut"},
                     {"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}
                   ]'
                             data-textAlign="['inherit','inherit','inherit','inherit']"
                             data-paddingtop="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]">
                            <img src="logo.png" alt="Baños termales & SPA en Aguascalientes Ojocaliente" width="1500"
                                 height="1500"
                                 data-ww="['500px','500px','400px','300px']"
                                 data-hh="['188px','188px','150px','113px']"
                                 data-no-retina>
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption   tp-svg-layer"
                             id="slide-418-layer-4"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['bottom','bottom','bottom','bottom']" data-voffset="['79','40','40','40']"
                             data-width="50"
                             data-height="50"
                             data-whitespace="nowrap"

                             data-type="svg"
                             data-actions='[{"event":"click","action":"scrollbelow","offset":"-90px","delay":"","speed":"300","ease":"Linear.easeNone"}]'
                             data-svg_src="assets/global/img/index/index-flecha-abajo.svg"
                             data-svg_idle="sc:transparent;sw:0;sda:0;sdo:0;"
                             data-svg_hover="sc:transparent;sw:0;sda:0;sdo:0;"
                             data-basealign="slide"
                             data-responsive_offset="off"
                             data-responsive="off"
                             data-frames='[{"delay":1500,"speed":1000,"frame":"0","from":"y:bottom;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"150","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bc:rgba(0, 0, 0, 1.00);"}]'
                             data-textAlign="['center','center','center','center']"
                             data-paddingtop="[7,7,7,7]"
                             data-paddingright="[5,5,5,5]"
                             data-paddingbottom="[5,5,5,5]"
                             data-paddingleft="[5,5,5,5]"

                             style="z-index: 8; min-width: 50px; max-width: 50px; max-width: 50px; max-width: 50px; color: rgba(0, 0, 0, 0.65);font-family:Open Sans;border-color:rgba(0, 0, 0, 0.65);border-style:solid;border-width:2px 2px 2px 2px;border-radius:40px 40px 40px 40px;cursor:pointer;"></div>

                    </li>
                </ul>

                <div class="tp-bannertimer" style="height: 6px; background-color: rgba(46, 209, 255, 1);"></div>
            </div>
        </div><!-- END REVOLUTION SLIDER -->
    </div>

    <div id="nosotros" class="g-pb-30  align-middle g-bg-cover g-pos-rel g-bg-white">
        <img src="assets/global/img/index/index-fondo-nosotros.jpg" class="img-fluid g-bg-cover g-hidden-sm-down">
        <div class="g-absolute-centered--md g-width-75x--md">
            <h2 class="g-font-size-70 g-mb-30 font-Dancing g-pt-140 text-center">Nosotros</h2>
            <center>
                <p class="g-font-size-18 g-px-10 text-md-center">Es uno de los lugares
                    más recomendables para liberarte de las preocupaciones y dejarte querer, las personas que se dan la
                    oportunidad de asistir terminan totalmente satisfechas
                    y cargadas de energía, salud y bienestar tanto mental como físico.
                    <br>
                    <br>
                    Haz a un lado el estrés y date una vuelta por este lugar en donde el feng shui y la meditación juegan un
                    papel importante durante toda tu estancia. Te recomiendo que realices
                    con anticipación la reservación para apartar tu lugar. No te pierdas la oportunidad y regálate unos días
                    de descanso y tranquilidad.</p>
            </center>
            <img src="assets/global/img/index/index-fondo-nosotros.jpg" class="img-fluid g-bg-cover g-hidden-sm-up">
        </div>
    </div>

    <!-- Section Servicios -->
    <section id="servicios" class="g-pt-100" style="background: url(assets/global/img/index/index-fondo-servicios.jpg)">
        <div class="container text-center g-width-1025 g-mb-65">
            <div class="u-heading-v7-3 g-mb-30">
                <h2 class="u-heading-v7__title g-font-size-70 g-mb-20 font-Dancing">Servicios de Belleza</span></h2>
            </div>
        </div>

        <div class="container">
            <div class="row">

                <div class="col-md-6 col-lg-4 g-mb-30">
                    <a href="#!">
                        <div class="g-parent g-pos-rel g-overflow-hidden">
                            <img class="img-fluid" src="assets/global/img/index/index-galeria-servicios-1.png"
                                 alt="Image description">
                            <div class="g-pos-abs g-top-0 g-left-0 g-absolute-centered w-75 h-75 opacity-0 g-opacity-1--parent-hover g-pa-15 g-transition-0_3 g-transition--ease-in">
                                <div class="d-flex align-items-center h-100 g-bg-primary-opacity-0_8" style="border-radius: 50%">
                                    <div class="w-100 text-center g-pa-30">
                                        <h3 class="g-font-weight-700 g-font-size-25  g-color-white g-mb-10 text-uppercase">
                                            Limpieza facial</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 g-mb-30">
                    <a href="#!">
                        <div class="g-parent g-pos-rel g-overflow-hidden">
                            <img class="img-fluid" src="assets/global/img/index/index-galeria-servicios-2.png"
                                 alt="Image description">
                            <div class="g-pos-abs g-top-0 g-left-0 g-absolute-centered w-75 h-75 opacity-0 g-opacity-1--parent-hover g-pa-15 g-transition-0_3 g-transition--ease-in">
                                <div class="d-flex align-items-center h-100 g-bg-primary-opacity-0_8" style="border-radius: 50%">
                                    <div class="w-100 text-center g-pa-30">
                                        <h3 class="g-font-weight-700 g-font-size-25  g-color-white g-mb-10 text-uppercase">
                                            Hydrafacial</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 g-mb-30">
                    <a href="#!">
                        <div class="g-parent g-pos-rel g-overflow-hidden">
                            <img class="img-fluid" src="assets/global/img/index/index-galeria-servicios-3.png"
                                 alt="Image description">
                            <div class="g-pos-abs g-top-0 g-left-0 g-absolute-centered w-75 h-75 opacity-0 g-opacity-1--parent-hover g-pa-15 g-transition-0_3 g-transition--ease-in">
                                <div class="d-flex align-items-center h-100 g-bg-primary-opacity-0_8" style="border-radius: 50%">
                                    <div class="w-100 text-center g-pa-30">
                                        <h3 class="g-font-weight-700 g-font-size-25  g-color-white g-mb-10 text-uppercase">
                                            Faciales</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 g-mb-30">
                    <a href="#!">
                        <div class="g-parent g-pos-rel g-overflow-hidden">
                            <img class="img-fluid" src="assets/global/img/index/index-galeria-servicios-4.png"
                                 alt="Image description">
                            <div class="g-pos-abs g-top-0 g-left-0 g-absolute-centered w-75 h-75 opacity-0 g-opacity-1--parent-hover g-pa-15 g-transition-0_3 g-transition--ease-in">
                                <div class="d-flex align-items-center h-100 g-bg-primary-opacity-0_8" style="border-radius: 50%">
                                    <div class="w-100 text-center g-pa-30">
                                        <h3 class="g-font-weight-700 g-font-size-25  g-color-white g-mb-10 text-uppercase">
                                            Masaje relajante</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 g-mb-30">
                    <a href="#!">
                        <div class="g-parent g-pos-rel g-overflow-hidden">
                            <img class="img-fluid" src="assets/global/img/index/index-galeria-servicios-5.png"
                                 alt="Image description">
                            <div class="g-pos-abs g-top-0 g-left-0 g-absolute-centered w-75 h-75 opacity-0 g-opacity-1--parent-hover g-pa-15 g-transition-0_3 g-transition--ease-in">
                                <div class="d-flex align-items-center h-100 g-bg-primary-opacity-0_8" style="border-radius: 50%">
                                    <div class="w-100 text-center g-pa-30">
                                        <h3 class="g-font-weight-700 g-font-size-25  g-color-white g-mb-10 text-uppercase">
                                            Masaje adyuvedicos</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 g-mb-30">
                    <a href="#!">
                        <div class="g-parent g-pos-rel g-overflow-hidden">
                            <img class="img-fluid" src="assets/global/img/index/index-galeria-servicios-6.png"
                                 alt="Image description">
                            <div class="g-pos-abs g-top-0 g-left-0 g-absolute-centered w-75 h-75 opacity-0 g-opacity-1--parent-hover g-pa-15 g-transition-0_3 g-transition--ease-in">
                                <div class="d-flex align-items-center h-100 g-bg-primary-opacity-0_8" style="border-radius: 50%">
                                    <div class="w-100 text-center g-pa-30">
                                        <h3 class="g-font-weight-700 g-font-size-25  g-color-white g-mb-10 text-uppercase">
                                            Masaje de Cuarzos</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 g-mb-30">
                    <a href="#!">
                        <div class="g-parent g-pos-rel g-overflow-hidden">
                            <img class="img-fluid" src="assets/global/img/index/index-galeria-servicios-7.png"
                                 alt="Image description">
                            <div class="g-pos-abs g-top-0 g-left-0 g-absolute-centered w-75 h-75 opacity-0 g-opacity-1--parent-hover g-pa-15 g-transition-0_3 g-transition--ease-in">
                                <div class="d-flex align-items-center h-100 g-bg-primary-opacity-0_8" style="border-radius: 50%">
                                    <div class="w-100 text-center g-pa-30">
                                        <h3 class="g-font-weight-700 g-font-size-25  g-color-white g-mb-10 text-uppercase">
                                            Masajes reductivos</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 g-mb-30">
                    <a href="#!">
                        <div class="g-parent g-pos-rel g-overflow-hidden">
                            <img class="img-fluid" src="assets/global/img/index/index-galeria-servicios-8.png"
                                 alt="Image description">
                            <div class="g-pos-abs g-top-0 g-left-0 g-absolute-centered w-75 h-75 opacity-0 g-opacity-1--parent-hover g-pa-15 g-transition-0_3 g-transition--ease-in">
                                <div class="d-flex align-items-center h-100 g-bg-primary-opacity-0_8" style="border-radius: 50%">
                                    <div class="w-100 text-center g-pa-30">
                                        <h3 class="g-font-weight-700 g-font-size-25  g-color-white g-mb-10 text-uppercase">
                                            Slim taping</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 g-mb-30">
                    <a href="#!">
                        <div class="g-parent g-pos-rel g-overflow-hidden">
                            <img class="img-fluid" src="assets/global/img/index/index-galeria-servicios-9.png"
                                 alt="Image description">
                            <div class="g-pos-abs g-top-0 g-left-0 g-absolute-centered w-75 h-75 opacity-0 g-opacity-1--parent-hover g-pa-15 g-transition-0_3 g-transition--ease-in">
                                <div class="d-flex align-items-center h-100 g-bg-primary-opacity-0_8" style="border-radius: 50%">
                                    <div class="w-100 text-center g-pa-30">
                                        <h3 class="g-font-weight-700 g-font-size-25  g-color-white g-mb-10 text-uppercase">
                                            Baño de novia</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </section>
    <!-- End Section Servicios -->

    <section id="promociones-paquetes" class="g-theme-bg-gray-light-v1 g-py-80"  style="background: url(assets/global/img/index/index-fondo-paquetes.jpg)">
        <div class="container text-center  g-mb-50">
            <div class="g-mb-25">
                <h2 class="u-heading-v7__title g-font-size-70 g-mb-20 font-Dancing">Promociones y Paquetes</span></h2>
            </div>
        </div>

        <div class="container">
            <!-- Products Block -->
            <div class="row">

                <div class="col-md-6 g-mb-30">
                    <!-- Article -->
                    <article class="text-center g-bg-white">
                        <!-- Article Image -->
                        <div class="g-pos-rel">
                            <img class="w-100" src="assets/global/img/index/index-paquete-1.jpg" alt="Paquetes">

                            <div class="u-ribbon-v1 text-uppercase g-top-0 g-right-0 g-width-70 g-font-weight-700 g-font-size-16 g-bg-primary p-0">
                                <div class="g-pos-rel g-height-140 g-color-white g-py-5">
                                    <span class="g-absolute-centered ">P<br>
                                                                       R<br>
                                                                       O<br>
                                                                       M<br>
                                                                       O</span>
                                </div>
                            </div>
                        </div>
                        <!-- End Article Image -->

                        <!-- Article Content -->
                        <div class="u-shadow-v9 g-pa-30 g-height-300">
                            <header class="g-mb-15">
                                <h2 class="u-heading-v7__title g-font-size-30 g-mb-20 font-Dancing"><b>Limpieza
                                        Facial</b></span></h2>
                            </header>

                            <!-- Article Info -->
                            <h3 class="h6 text-uppercase g-line-height-1_8 g-font-weight-700 g-mb-20">Limpia tu piel
                                suavemente y mantén el pH tu piel.</h3>
                            <p class="mb-0">Una de las mejores rutinas de cuidado personal que podemos adquirir desde
                                una edad temprana, es realizarnos limpiezas faciales con nosotros.</p>
                            <!-- End Article Info -->
                        </div>
                        <!-- End Article Content -->
                    </article>
                    <!-- End Article -->
                </div>

                <div class="col-md-6 g-mb-30">
                    <!-- Article -->
                    <article class="text-center g-bg-white">
                        <!-- Article Image -->
                        <div class="g-pos-rel">
                            <img class="w-100" src="assets/global/img/index/index-paquete-2.jpg" alt="Paquetes">

                            <div class="u-ribbon-v1 text-uppercase g-top-0 g-right-0 g-width-70 g-font-weight-700 g-font-size-16 g-bg-primary p-0">
                                <div class="g-pos-rel g-height-140 g-color-white g-py-5">
                                    <span class="g-absolute-centered ">P<br>
                                                                       R<br>
                                                                       O<br>
                                                                       M<br>
                                                                       O</span>
                                </div>
                            </div>
                        </div>
                        <!-- End Article Image -->

                        <!-- Article Content -->
                        <div class="u-shadow-v9 g-pa-30 g-height-300">
                            <header class="g-mb-15">
                                <h2 class="u-heading-v7__title g-font-size-30 g-mb-20 font-Dancing"><b>Masaje
                                        Relajante</b></span></h2>
                            </header>

                            <!-- Article Info -->
                            <h3 class="h6 text-uppercase g-line-height-1_8 g-font-weight-700 g-mb-20">Un masaje
                                anti-estrés es una forma de ayudarte a liberar las tensiones y aliviar dolores.</h3>
                            <p class="mb-0">Masaje Relajante es una terapia manual destinada a mejorar el bienestar de
                                la persona, ya que su máximo objetivo es aumentar la producción de endorfinas.</p>
                            <!-- End Article Info -->
                        </div>
                        <!-- End Article Content -->
                    </article>
                    <!-- End Article -->
                </div>


            </div>

            <div class="row">

                <div class="col-md-6 col-lg-3 g-mb-30 g-mb-0--lg">
                    <!-- Article -->
                    <article class="text-center g-bg-white">
                        <!-- Article Image -->
                        <div class="g-pos-rel">
                            <img class="w-100" src="assets/global/img/index/index-paquete-3.jpg"
                                 alt="Image description">

                            <div class="u-ribbon-v1 text-uppercase g-top-0 g-left-0 g-width-60 g-font-weight-700 g-font-size-15 g-bg-primary p-0">
                                <div class="g-pos-rel g-height-120 g-color-white g-py-15">
                                    <span class="g-absolute-centered">N<br>
                                                                       U<br>
                                                                       E<br>
                                                                       V<br>
                                                                       O</span>
                                </div>
                            </div>
                        </div>
                        <!-- End Article Image -->

                        <!-- Article Content -->
                        <div class="u-shadow-v9 g-py-30 g-px-20 g-height-200--lg g-height-300">
                            <header class="g-mb-15">
                                <h2 class="u-heading-v7__title g-font-size-20 g-mb-20 font-Dancing"><b>Servicio
                                        1</b></span></h2>
                            </header>

                            <!-- Article Info -->
                            <h3 class="text-uppercase g-line-height-1_8 g-font-weight-700 g-font-size-12 g-mb-20">
                                Encabezado</h3>
                            <p class="g-font-size-13 mb-0">Descripción</p>
                            <!-- End Article Info -->
                        </div>
                        <!-- End Article Content -->
                    </article>
                    <!-- End Article -->
                </div>

                <div class="col-md-6 col-lg-3 g-mb-30 g-mb-0--lg">
                    <!-- Article -->
                    <article class="text-center g-bg-white">
                        <!-- Article Image -->
                        <div class="g-pos-rel">
                            <img class="w-100" src="assets/global/img/index/index-paquete-4.jpg"
                                 alt="Image description">

                            <div class="u-ribbon-v1 text-uppercase g-top-0 g-left-0 g-width-60 g-font-weight-700 g-font-size-15 g-bg-primary p-0">
                                <div class="g-pos-rel g-height-120 g-color-white g-py-15">
                                    <span class="g-absolute-centered">N<br>
                                                                       U<br>
                                                                       E<br>
                                                                       V<br>
                                                                       O</span>
                                </div>
                            </div>
                        </div>
                        <!-- End Article Image -->

                        <!-- Article Content -->
                        <div class="u-shadow-v9 g-py-30 g-px-20 g-height-200--lg g-height-300">
                            <header class="g-mb-15">
                                <h2 class="u-heading-v7__title g-font-size-20 g-mb-20 font-Dancing"><b>Servicio
                                        1</b></span></h2>
                            </header>

                            <!-- Article Info -->
                            <h3 class="text-uppercase g-line-height-1_8 g-font-weight-700 g-font-size-12 g-mb-20">
                                Encabezado</h3>
                            <p class="g-font-size-13 mb-0">Descripción</p>
                            <!-- End Article Info -->
                        </div>
                        <!-- End Article Content -->
                    </article>
                    <!-- End Article -->
                </div>

                <div class="col-md-6 col-lg-3 g-mb-30 g-mb-0--lg">
                    <!-- Article -->
                    <article class="text-center g-bg-white">
                        <!-- Article Image -->
                        <div class="g-pos-rel">
                            <img class="w-100" src="assets/global/img/index/index-paquete-5.jpg"
                                 alt="Image description">

                            <div class="u-ribbon-v1 text-uppercase g-top-0 g-left-0 g-width-60 g-font-weight-700 g-font-size-15 g-bg-primary p-0">
                                <div class="g-pos-rel g-height-120 g-color-white g-py-15">
                                    <span class="g-absolute-centered">N<br>
                                                                       U<br>
                                                                       E<br>
                                                                       V<br>
                                                                       O</span>
                                </div>
                            </div>
                        </div>
                        <!-- End Article Image -->

                        <!-- Article Content -->
                        <div class="u-shadow-v9 g-py-30 g-px-20 g-height-200--lg g-height-300">
                            <header class="g-mb-15">
                                <h2 class="u-heading-v7__title g-font-size-20 g-mb-20 font-Dancing"><b>Servicio
                                        1</b></span></h2>
                            </header>

                            <!-- Article Info -->
                            <h3 class="text-uppercase g-line-height-1_8 g-font-weight-700 g-font-size-12 g-mb-20">
                                Encabezado</h3>
                            <p class="g-font-size-13 mb-0">Descripción</p>
                            <!-- End Article Info -->
                        </div>
                        <!-- End Article Content -->
                    </article>
                    <!-- End Article -->
                </div>

                <div class="col-md-6 col-lg-3 g-mb-30 g-mb-0--lg">
                    <!-- Article -->
                    <article class="text-center g-bg-white">
                        <!-- Article Image -->
                        <div class="g-pos-rel">
                            <img class="w-100" src="assets/global/img/index/index-paquete-6.jpg"
                                 alt="Image description">

                            <div class="u-ribbon-v1 text-uppercase g-top-0 g-left-0 g-width-60 g-font-weight-700 g-font-size-15 g-bg-primary p-0">
                                <div class="g-pos-rel g-height-120 g-color-white g-py-15">
                                    <span class="g-absolute-centered">N<br>
                                                                       U<br>
                                                                       E<br>
                                                                       V<br>
                                                                       O</span>
                                </div>
                            </div>
                        </div>
                        <!-- End Article Image -->

                        <!-- Article Content -->
                        <div class="u-shadow-v9 g-py-30 g-px-20 g-height-200--lg g-height-300">
                            <header class="g-mb-15">
                                <h2 class="u-heading-v7__title g-font-size-20 g-mb-20 font-Dancing"><b>Servicio
                                        1</b></span></h2>
                            </header>

                            <!-- Article Info -->
                            <h3 class="text-uppercase g-line-height-1_8 g-font-weight-700 g-font-size-12 g-mb-20">
                                Encabezado</h3>
                            <p class="g-font-size-13 mb-0">Descripción</p>
                            <!-- End Article Info -->
                        </div>
                        <!-- End Article Content -->
                    </article>
                    <!-- End Article -->
                </div>

            </div>
            <!-- End Products Block -->
        </div>
    </section>

    <div id="cotizar"  style="background-color: #F5F5F5">
        <section class="g-pt-100">
            <br>
            <br>
            <br>
            <div id="shortcode2">
                <div class="shortcode-html">
                    <!-- Find a Course -->
                    <div class="g-bg-img-hero g-bg-pos-top-center g-pos-rel g-z-index-1 g-mt-minus-150"
                         style="background-image: url(assets/global/img/index/index-fondo-cotizar.jpg);">
                        <div class="container text-center  g-mb-50">
                            <div class="g-mb-25">
                                <h2 class="u-heading-v7__title g-font-size-70 g-pt-50 font-Dancing">Agendar
                                    Cita</span></h2>
                            </div>
                        </div>
                        <div class="container g-pt-70 g-pb-30">
                            <form class="row">
                                <div class="col-xl-8 g-mb-30">
                                    <div class="g-mb-50">
                                        <label class="g-font-weight-500 g-font-size-15 g-pl-30">SOLICITUD:</label>
                                        <input id="inp-solicitud"
                                               class="form-control u-shadow-v19 g-brd-none g-bg-white g-font-size-16 g-rounded-30 g-px-30 g-py-13 g-mb-30"
                                               type="text" placeholder="Explica el tipo de servicio que necesitas.">
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6 g-mb-50">
                                            <!-- Area of Interest -->
                                            <label class="g-font-weight-500 g-font-size-15 g-pl-30">SERVICIOS:</label>
                                            <select id="select-servicios"
                                                    class="form-control g-rounded-30 g-pl-30 g-py-12"
                                                    data-placeholder="SERVICIOS:">
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="">Elige uno
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="Limpieza Facial">Limpieza Facial
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="Hydrafacial">Hydrafacial
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="Faciales">Faciales
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="Masaje Relajante">Masaje Relajante
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="Masaje Adyuvedicos">Masaje Adyuvedicos
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="Masaje de Cuarzos">Masaje de Cuarzos
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="Masajes Reductivos">Masajes Reductivos
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="Slim Taping">Slim Taping
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="Baño de Novia">Baño de Novia
                                                </option>
                                            </select>
                                            <!-- End Area of Interest -->
                                        </div>

                                        <div class="col-sm-6 g-mb-50">
                                            <!-- Type -->
                                            <label class="g-font-weight-500 g-font-size-15 g-pl-30">PARA:</label>
                                            <select id="select-para" class="form-control g-rounded-30 g-pl-30 g-py-12"
                                                    data-placeholder="PARA:">
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="">Elige uno
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="1 Persona">1 Persona
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="2 Personas">2 Personas
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="3 Personas">3 Personas
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="4 Personas">4 Personas
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-black g-color-white--active g-bg-primary--active"
                                                        value="5 Personas">5 Personas
                                                </option>
                                            </select>
                                        </div>

                                        <div class="col-xl-12">
                                            <label class="g-font-weight-500 g-font-size-15 g-pl-30">Nombre:</label>
                                            <input id="inp-nombre"
                                                   class="form-control u-shadow-v19 g-brd-none g-bg-white g-font-size-16 g-rounded-30 g-px-30 g-py-13 g-mb-30"
                                                   type="text" placeholder="Deja tu nombre completo.">
                                        </div>

                                        <div class="col-sm-6 g-mb-30">
                                            <label class="g-font-weight-500 g-font-size-15 g-pl-30">Teléfono (10
                                                Dígitos):</label>
                                            <input id="inp-telefono"
                                                   class="form-control u-shadow-v19 g-brd-none g-bg-white g-font-size-16 g-rounded-30 g-px-30 g-py-13 g-mb-30"
                                                   type="text" placeholder="Deja tu teléfono de contacto.">
                                        </div>

                                        <div class="col-sm-6 g-mb-30">
                                            <label class="g-font-weight-500 g-font-size-15 g-pl-30">Correo:</label>
                                            <input id="inp-correo"
                                                   class="form-control u-shadow-v19 g-brd-none g-bg-white g-font-size-16 g-rounded-30 g-px-30 g-py-13 g-mb-30"
                                                   type="text" placeholder="Deja tu correo de contacto.">
                                        </div>

                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-6 g-mb-50 text-center">
                                            <div class="d-flex">
                                                <button class="btn btn-md btn-block text-uppercase u-btn-primary g g-font-size-16 g-rounded-30 g-py-10 ml-2 g-mt-0 g-bg-primary--hover"
                                                        onclick="obtener_datos_cotizacion()" href="#!" type="button">COTIZAR
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-sm-3"></div>


                                        <!-- Se podría utilizar
                                        <div class="col-sm-6 g-mt-30 g-mb-30">
                                            <div class="d-flex">
                                                <button class="btn btn-block u-shadow-v32 g-brd-black g-brd-2 g-color-black g-color-white--hover g-bg-transparent g-bg-black--hover g-font-size-16 g-rounded-30 g-py-10 mr-2 g-mt-0" type="button">RESET</button>
                                                <button class="btn btn-block u-shadow-v32 g-brd-none g-color-white g-bg-black g-bg-primary--hover g-font-size-16 g-rounded-30 g-py-10 ml-2 g-mt-0" type="button">COTIZAR</button>
                                            </div>
                                        </div>
                                        -->

                                    </div>
                                </div>

                                <div class="col-xl-4 g-mb-30">
                                    <!-- Datepicker -->
                                    <label class="g-font-weight-500 g-font-size-15">FECHA PARA LA CITA:</label>
                                    <div id="datepickerInline"
                                         class="u-datepicker-v1 u-shadow-v32 g-brd-none rounded"></div>
                                    <!-- End Datepicker -->
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End Find a Course -->
                </div>
        </section>
    </div>

    <section id="contacto" class="g-py-70" style="background-color: #F5F5F5">
        <div class="container text-center g-width-590 g-mb-50">
            <div class="container text-center g-width-1025 g-mb-65">
                <div class="u-heading-v7-3 g-mb-30">
                    <h2 class="u-heading-v7__title g-font-size-70 g-mb-20 font-Dancing">Preguntanos tus dudas</span></h2>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-9 g-mb-40 g-mb-0--md">
                    <form>
                        <div class="form-group g-mb-25">
                            <input id="inp-nombre-contacto" class="form-control h-100 g-color-gray-dark-v5 g-placeholder-inherit g-bg-white g-brd-none rounded-0 g-pa-12" type="text" placeholder="Nombre">
                        </div>

                        <div class="form-group g-mb-25">
                            <input id="inp-correo-contacto" class="form-control h-100 g-color-gray-dark-v5 g-placeholder-inherit g-bg-white g-brd-none rounded-0 g-pa-12" type="tel" placeholder="Email">
                        </div>

                        <div class="form-group g-mb-25">
                            <textarea id="inp-mensaje-contacto" class="form-control g-resize-none g-color-gray-dark-v5 g-placeholder-inherit g-bg-white g-brd-none rounded-0 g-py-6 g-px-12" rows="5" placeholder="Mensaje"></textarea>
                        </div>

                        <button class="btn btn-md text-uppercase btn-block u-btn-outline-primary g-font-weight-700 g-font-size-11 g-brd-2 rounded-0 g-py-19 g-px-20" type="button"
                                onclick="obtener_datos_contacto()">Enviar Datos</button>
                    </form>
                </div>

                <div class="col-md-3 text-center text-md-left g-mb-40--md">
                    <div class="g-mb-25">
                        <span class="u-icon-v1 u-icon-size--xs g-color-primary">
                            <i class="fa fa-map-marker"></i>
                        </span>
                        <em class="d-block g-color-black-light-v2 text-uppercase g-font-size-12 g-font-style-normal g-mb-5">Dirección</em>
                        <a class="g-color-black g-color-primary--hover g-color-black--focus"
                           href="https://www.google.com/maps/place/La+Plazita/@21.9245838,-102.3183554,16z/data=!4m8!1m2!2m1!1sLa+Plazita+local+13.+Blvd.+Luis+Donaldo+Colosio+Murieta+%23400!3m4!1s0x0:0x2e2cd7bbe6bb9330!8m2!3d21.9248404!4d-102.310752"
                           target="_blank">
                            <span> La Plazita local 13. Blvd. Luis Donaldo Colosio Murieta #400 <br> 20110 Aguascalientes</span>
                        </a>
                    </div>
                    <div class="g-mb-25">
                        <span class="u-icon-v1 u-icon-size--xs g-color-primary">
                            <i class="fa fa-phone-square"></i>
                        </span>
                        <em class="d-block g-color-black-light-v2 text-uppercase g-font-size-12 g-font-style-normal g-mb-5">Teléfono</em>
                        <a class="g-color-black g-color-primary--hover g-color-black--focus" href="tel:4499031636">
                            <span>449 903 1636</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer id="mapa" style="background-color: #F5F5F5">
        <!-- Google Map -->
        <div id="gMap" class="js-g-map g-min-height-450 h-100"
             data-type="custom"
             data-lat="21.9245199"
             data-lng="-102.3107339"
             data-zoom="16"
             data-title="Travel"
             data-styles='[
               ["", "", [{"gamma":1},{"weight":1},{"visibility":"simplified"},{"hue":"#D7A09D"}]],
               ["", "labels", [{"visibility":"on"}]],
               ["water", "", [{"color":"#aee2e0"}]]
             ]'
             data-pin="true"
             data-pin-icon="pin-teal.png"></div>
        <!-- End Google Map -->

        <div class="container-fluid text-center g-color-gray-dark-v5 g-py-40">
            <ul class="list-inline d-inline-block g-mb-30">
                <li class="list-inline-item g-mr-10">
                    <a class="u-icon-v3 g-width-35 g-height-35 g-font-size-15 g-color-gray-dark-v2 g-color-white--hover g-bg-primary--hover g-transition-0_2 g-transition--ease-in"
                       href="https://www.facebook.com/pages/category/Health-Spa/Perfeczzione-105530587468679/"
                       target="_blank"><i class="fa fa-facebook"></i></a>
                </li>
                <li class="list-inline-item g-mr-10">
                    <a class="u-icon-v3 g-width-35 g-height-35 g-font-size-15 g-color-gray-dark-v2 g-color-white--hover g-bg-primary--hover g-transition-0_2 g-transition--ease-in"
                       href="https://www.instagram.com/perfeczzione.ags/"
                       target="_blank"><i class="fa fa-instagram"></i></a>
                </li>
            </ul>
            <hr class="g-color-primary g-bg-primary g-width-50x">
            <ul class="list-inline text-uppercase g-font-weight-700 g-font-size-11 mb-0">
                <li class="list-inline-item g-px-12--md">
                    <a class="g-color-gray-dark-v5 g-color-primary--hover g-text-underline--none--hover"
                       href="https://maindsoft.net/" target="_blank">
                        <img src="assets/global/img/index/logo-maindsoft.png" height="30">
                    </a>
                </li>
            </ul>
        </div>
    </footer>
    <!-- End Footer -->

</main>
</body>

<!-- JS Global Compulsory -->

<!-- jquery -->
<script src="assets/global/js/jquery/jquery.min.js"></script>
<script src="assets/global/js/jquery/jquery-migrate.min.js"></script>
<script src="assets/global/js/jquery/jquery.easing/js/jquery.easing.js"></script>

<!-- popper -->
<script src="assets/global/js/popper/popper.min.js"></script>

<!-- bootstrap -->
<script src="assets/global/js/bootstrap/bootstrap.min.js"></script>

<!-- Components -->
<script src="assets/global/js/hs.core.js"></script>

<!-- Map -->
<script src="assets/global/js/map/gmaps.min.js"></script>
<script src="assets/global/js/map/hs.map.js"></script>

<!-- Header -->
<script src="assets/global/js/header/hs.header.js"></script>
<script src="assets/global/js/hamburgers/hs.hamburgers.js"></script>

<!-- FancyBox -->
<script src="assets/global/js/fancybox/jquery.fancybox.min.js"></script>
<script src="assets/global/js/popup/hs.popup.js"></script>

<!-- select -->
<script src="assets/global/js/hs-select/hs.select.js"></script>

<!-- JS Implementing Plugins -->
<script src="assets/global/chosen/chosen.jquery.js"></script>

<!-- datepicker -->
<script src="assets/global/js/datepicker/datepicker.js"></script>
<script src="assets/global/js/datepicker/hs.datepicker.js"></script>

<!-- JS Revolution Slider -->
<script src="assets/global/js/revolution-slider/jquery.themepunch.tools.min.js"></script>
<script src="assets/global/js/revolution-slider/jquery.themepunch.revolution.min.js"></script>
<script src="assets/global/js/revolution-slider/extensions/revolution.extension.actions.min.js"></script>
<script src="assets/global/js/revolution-slider/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="assets/global/js/revolution-slider/extensions/revolution.extension.navigation.min.js"></script>
<script src="assets/global/js/revolution-slider/extensions/revolution.extension.parallax.min.js"></script>
<script src="assets/global/js/revolution-slider/extensions/revolution.extension.slideanims.min.js"></script>
<script src="assets/global/js/revolution-slider/extensions/revolution.extension.video.min.js"></script>

<!-- Custom -->
<script src="assets/global/js/custom.js"></script>
<script src="assets/global/js/rules.js"></script>

<!-- JS Plugins Init. -->
<script>

    // initialization of google map
    function initMap() {
        $.HSCore.components.HSGMap.init('.js-g-map');
    }

    $(document).on('ready', function () {
        // Header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');
        $.HSCore.components.HSPopup.init('.js-fancybox');

        // initialization of forms
        $.HSCore.components.HSDatepicker.init('#datepickerInline');
    });

    //Inicializando el Slider Principal
    var tpj = jQuery;
    var revapi1063;
    tpj(document).ready(function () {
        if (tpj('#rev_slider_1063_1').revolution == undefined) {
            revslider_showDoubleJqueryError('#rev_slider_1063_1');
        } else {
            revapi1063 = tpj('#rev_slider_1063_1').show().revolution({
                sliderType: 'standard',
                jsFileLocation: 'revolution/js/',
                sliderLayout: 'fullscreen',
                dottedOverlay: 'none',
                delay: 9000,
                navigation: {
                    keyboardNavigation: 'off',
                    keyboard_direction: 'horizontal',
                    mouseScrollNavigation: 'off',
                    mouseScrollReverse: 'default',
                    onHoverStop: 'off',
                    touch: {
                        touchenabled: 'on',
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: 'horizontal',
                        drag_block_vertical: false
                    }
                    ,

                },
                viewPort: {
                    enable: true,
                    outof: 'pause',
                    visible_area: '80%',
                    presize: false
                },
                responsiveLevels: [1240, 1024, 778, 480],
                visibilityLevels: [1240, 1024, 778, 480],
                gridwidth: [1230, 1024, 767, 480],
                gridheight: [720, 720, 480, 360],
                lazyType: 'none',
                parallax: {
                    type: 'scroll',
                    origo: 'enterpoint',
                    speed: 400,
                    levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 46, 47, 48, 49, 50, 55]
                },
                shadow: 0,
                spinner: 'off',
                stopLoop: 'off',
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: 'off',
                autoHeight: 'off',
                hideThumbsOnMobile: 'off',
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: 'off',
                    nextSlideOnWindowFocus: 'off',
                    disableFocusListener: false
                }
            });
        }
    });


    function href_div_scroll(id_div) {
        $("html, body").animate({scrollTop: $('#' + id_div).offset().top}, 1000);
    }

</script>

<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBmQYhY3TRKgiY-qbwWuIemSzdVLlVigVI&callback=initMap" async
        defer></script>

</html>