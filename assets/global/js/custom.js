/**
 * custom.js
 * Autor: Luis Cristerna (Maindsoft)
 * Fecha: 22/01/2018
 */

/**
 * Metodo que nos ayuda a borrar toda la informacon que se tiene en el form
 */
function resetForm(elementos_form) {
    var i;
    for (i = 0; i < elementos_form.length; i++){
        document.getElementById(elementos_form[i]).value = "";
    }

}


function cambiar_posicion_imagen_slider_principal_nosotros(px) {
    if (px.matches) {
        $("#img-esponja-nosotros").attr("data-offset-y","-450");
        $("#btn-conoce-mas-nosotros").attr("data-offset-y","550");

    } else {
        $("#img-esponja-nosotros").attr("data-offset-y","-225");

    }
}


function cambio_de_posicion_imagen() {
    var px = window.matchMedia("(max-width: 479px)")
    cambiar_posicion_imagen_slider_principal_nosotros(px) // Call listener function at run time
    px.addListener(cambiar_posicion_imagen_slider_principal_nosotros) // Attach listener function on state changes
}


/**
 * Metodo exclusivo del carousel de las lineas de negocio, el cual remplaza las imagenes segun el dispositivo en el que se navegue
 * @param px
 */
function remplazar_imagen_slider_principal_index(px) {
    if (px.matches) {
        $("#img-manos-limpiando").attr("data-lazyload","assets/img/index/index-mano-limpiando-slider-principal.png");
        $("#img-limpiador-esponja").attr("data-lazyload","assets/img/index/dummy.png");
        $("#img-cepillo").attr("data-lazyload","assets/img/index/dummy.png");
        $("#img-recogedor").attr("data-lazyload","assets/img/index/dummy.png");

    } else {
        $("#img-manos-limpiando").attr("data-lazyload","assets/img/index/index-manos-limpiando-slider-principal.png");
        $("#img-limpiador-esponja").attr("data-lazyload","assets/img/index/index-limpiador-esponja-slider-principal.png");
        $("#img-cepillo").attr("data-lazyload","assets/img/index/index-cepillo-limpiador-slider-principal.png");
        $("#img-recogedor").attr("data-lazyload","assets/img/index/index-recogedor-slider-principal.png");
    }
}

/**
 * Metodo que invoca remplazar_imagen_movil_linea_negocio, debido a que se manda llamar para un archivo php y
 * no se puede llamar directamente en ese lenguaje
 */
function cambio_de_imagen_slider_principal_index() {
    var px = window.matchMedia("(max-width: 479px)")
    remplazar_imagen_slider_principal_index(px) // Call listener function at run time
    px.addListener(remplazar_imagen_slider_principal_index) // Attach listener function on state changes
}

function cambio_imagen_segun_px(atributo, id_img, src_img_1, src_img_2, px) {
    var pixels = window.matchMedia("(max-width: " + px+ "px)");
    remplazar_imagen_segun_px(atributo, id_img, src_img_1, src_img_2, pixels);
    pixels.addListener(remplazar_imagen_segun_px);

}

function remplazar_imagen_segun_px(atributo, id_img, src_img_1, src_img_2, pixels) {
    if (pixels.matches){
        $("#" + id_img).attr(atributo, src_img_1);
    } else {
        $("#" + id_img).attr(atributo, src_img_2);
    }
}

function obtener_datos_contacto() {
    inp_nombre_contacto = $('#inp-nombre-contacto').val();
    inp_mensaje_contacto = $('#inp-mensaje-contacto').val();
    inp_correo_contacto = $('#inp-correo-contacto').val();

    validar_campos_contacto(inp_nombre_contacto,inp_mensaje_contacto,inp_correo_contacto);
}

function validar_campos_contacto(nombre,mensaje,correo) {

    if (is_campo_vacio(mensaje, "Mensaje") == false || is_campo_corto(mensaje, "Mensaje", 10) ||
        is_campo_vacio(nombre, "Nombre") == false || is_campo_corto(nombre, "Nombre", 5) ||
        is_campo_vacio(correo, "Correo") == false || is_email(correo, "Correo") == false
    ){
    } else {
        enviar_datos_contacto(nombre,mensaje,correo);
    }
}

function enviar_datos_contacto(nombre,mensaje,correo) {
    var datos_cotizacion = new FormData();
    datos_cotizacion.append('NOMBRE',nombre);
    datos_cotizacion.append('MENSAJE',mensaje);
    datos_cotizacion.append('CORREO',correo);
    $.ajax({
        url: 'php/enviar-correo-contacto.php',
        type: 'post',
        data:  datos_cotizacion,
        contentType: false,
        processData: false,
        success: function (resultado) {
            if (resultado == 1) {
                alert("Estimado usuario:\n" +
                    "\n" +
                    "Agradecemos tu interés en conocer de cerca más sobre nosotros, Perfeczzione " +
                    "revisará tus datos y se pondrán en contacto muy pronto contigo.");
                location.reload();
            } else {
                alert(resultado);
            }
        }
    });
}

function obtener_datos_cotizacion() {
    inp_nombre = $('#inp-nombre').val();
    inp_solicitud = $('#inp-solicitud').val();
    inp_correo = $('#inp-correo').val();
    inp_telefono = $('#inp-telefono').val();
    select_servicios = $('#select-servicios').val();
    select_para = $('#select-para').val();
    if ($('#datepickerInline').val() == null){
        fecha = "Sin Definir";
    }  else {
        fecha = $('#datepickerInline').val();

    }

    validar_campos_cotizacion(inp_nombre,inp_solicitud, select_servicios, select_para, inp_correo, inp_telefono, fecha);

}

function validar_campos_cotizacion(nombre, solicitud, servicio, para, correo, telefono, fecha) {

    if (is_campo_vacio(solicitud, "Solicitud") == false || is_campo_corto(solicitud, "Solicitud", 10) ||
        is_campo_vacio(servicio, "Servicio") == false || is_campo_vacio(para, "Para") == false ||
        is_campo_vacio(nombre, "Nombre") == false || is_campo_corto(nombre, "Nombre", 5) ||
        is_campo_vacio(correo, "Correo") == false || is_email(correo, "Correo") == false ||
        is_campo_vacio(telefono, "Telefono") == false || is_telefono(telefono, "Telefono") == false
        ){
    } else {
        enviar_datos_cotizacion(nombre,solicitud, servicio, para, correo, telefono, fecha);
    }
}

function enviar_datos_cotizacion(nombre,solicitud,servicio, para, correo, telefono) {
    var datos_cotizacion = new FormData();
    datos_cotizacion.append('NOMBRE',nombre);
    datos_cotizacion.append('SOLICITUD',solicitud);
    datos_cotizacion.append('SERVICIO',servicio);
    datos_cotizacion.append('SERVICIO',servicio);
    datos_cotizacion.append('PARA',para);
    datos_cotizacion.append('CORREO',correo);
    datos_cotizacion.append('TELEFONO',telefono);
    datos_cotizacion.append('FECHA',fecha);
    $.ajax({
        url: 'php/enviar-correo.php',
        type: 'post',
        data:  datos_cotizacion,
        contentType: false,
        processData: false,
        success: function (resultado) {
            if (resultado == 1) {
                alert("Estimado usuario:\n" +
                    "\n" +
                    "Agradecemos tu interés en conocer de cerca nuestros servicios, Perfeczzione " +
                    "revisará tus datos y se pondrán en contacto muy pronto contigo.");
                location.reload();
            } else {
                alert(resultado);
            }
        }
    });

}