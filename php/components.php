<?php

function PrintGalery(){
    for ($i = 1; $i < 21; $i++){
        echo "<div class=\"ms-slide\" data-delay=\"5.6\" data-fill-mode=\"fill\">
                <img src=\"assets/global/img/index/index-img-blank.gif\" alt=\"Index | Mezquite\" title=\"business-bg-slide1\" data-src=\"assets/global/img/index/index-mezquite-galeria-$i.jpg\" />
                <div class=\"ms-layer font-title-55 msp-cn-163-1 g-font-family-adobe-jenson g-color-mezquite-paragraph\"
                 style=\"\"
                 data-effect=\"t(true,n,-30,n,-25,n,n,n,2,2,n,n,n,n,n)\"
                 data-duration=\"2500\"
                 data-delay=\"1000\"
                 data-ease=\"easeOutQuint\"
                 data-parallax=\"3\"
                 data-offset-x=\"0\"
                 data-offset-y=\"60\"
                 data-origin=\"mc\"
                 data-position=\"normal\">
            </div>
        </div>";
    }
}