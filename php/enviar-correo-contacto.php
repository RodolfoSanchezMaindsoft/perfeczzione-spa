<?php
$nombre = $_POST['NOMBRE'];
$solicitud = $_POST['MENSAJE'];
$correo = $_POST['CORREO'];

if ($nombre != null) {
    $para = 'contacto@perfeczzione.mx';

    $titulo = 'NUEVA SOLICITUD DE CONTACTO';

    $mensaje = '<!DOCTYPE html>
                            <html>
                            <head>
                                <style>
                                    #customers {
                                        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                                        border-collapse: collapse;
                                        width: 100%;
                                    }
                            
                                    #customers td, #customers th {
                                        border: 1px solid #ddd;
                                        padding: 8px;
                                    }
                            
                                    #customers tr:nth-child(even){background-color: #f2f2f2;}
                            
                                    #customers tr:hover {background-color: #ddd;}
                            
                                    #customers th {
                                        padding-top: 12px;
                                        padding-bottom: 12px;
                                        text-align: left;
                                        background-color: #D7A09D;
                                        color: white;
                                    }
                                    
                                    #datos-solicitados {
                                        width: 30% !important;
                                    }
                                   
                                    #info-recibida {
                                    width: 70% !important;
                                    }
                                </style>
                            </head>
                            <body>
                            
                            <center>
                                <img src="https://perfeczzione.mx/logo.png" alt="Logo">
                            </center>
                            <h1>Duda de: ' . $nombre . ' </h1>
                            
                            <table id="customers">
                                <tr>
                                    <th id="datos-solicitados">Datos Solicitados:</th>
                                    <th id="info-recibida">Información Recibida:</th>
                                </tr>
                                <tr>
                                    <td>Nombre del Solicitante:</td>
                                    <td>' . $nombre . '</td>
                                </tr>
                                <tr>
                                    <td>Correo del Solicitante:</td>
                                    <td>' . $correo . '</td>
                                </tr>
                                <tr>
                                    <td>Comentario del solicitante:</td>
                                    <td>' . $solicitud . '</td>
                                </tr>
                            </table>
                            
                            </body>
                            </html>';

    $cabeceras = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $cabeceras .= 'From: perfeczzione.mx';

    $enviado = mail($para, $titulo, $mensaje, $cabeceras);


    if ($enviado) {
        echo '1';
    } else {

        echo 'Error en el envío del email';
    }

}